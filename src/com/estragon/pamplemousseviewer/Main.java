package com.estragon.pamplemousseviewer;


import greendroid.app.GDListActivity;
import greendroid.widget.ActionBarItem;
import greendroid.widget.ActionBarItem.Type;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.estragon.pamplemousseviewer.parse.Evenement;
import com.estragon.pamplemousseviewer.util.requetes.MiseAJour;
import com.estragon.pamplemousseviewer.util.requetes.MiseAJour.MAJListener;



public class Main extends GDListActivity implements MAJListener {
	
	public static String FILENAME = "pamplemousse.json";
	
	public static final ArrayList<Object> EVENTS = new ArrayList<Object>();
	ArrayAdapter adapter;
	ProgressDialog dialog = null;

	public void afficherEmploiDuTemps() {
		String path = this.getFilesDir().getAbsolutePath()+"/"+FILENAME;
		List<Evenement> events = new ArrayList<Evenement>();
		try {
			FileReader fr = new FileReader(path);
			BufferedReader br = new BufferedReader(fr);
			StringBuilder builder = new StringBuilder();
			String line;
			while ((line = br.readLine())!= null) {
				builder.append(line);
			}
			JSONArray evenements = new JSONObject(builder.toString()).getJSONArray("events");
			for (int i = 0; i < evenements.length(); i++) {
				try {
					events.add(new Evenement(evenements.getJSONObject(i)));
				}
				catch (Exception e) {

				}
			}
		}
		catch (FileNotFoundException e) {
			//Le fichier n'existe pas, np
		}
		catch (IOException e) {
			
		}
		catch (JSONException e) {
			Toast.makeText(this, "JSON malformé :(:(", Toast.LENGTH_LONG).show();
		}
		EVENTS.clear();
		if (events.size() == 0 && !Prefs.isParametre(this)) EVENTS.add("Pour commencer, touchez le bouton menu en haut à droite et renseignez votre identifiant et votre mot de passe.");
		else {
			int previous = -1;
			for (Evenement e : events) {
				if (previous != e.getDebut().getDate())  {
					String sep = "";
					sep += Evenement.JOURS[e.getDebut().getDay()-1];
					sep += " "+e.getDebut().getDate();
					sep += " "+Evenement.MOIS[e.getDebut().getMonth()];
					EVENTS.add(sep);
				}
				previous = e.getDebut().getDate();
				EVENTS.add(e);
			}
		}
		adapter.notifyDataSetChanged();
	}

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getActionBar().removeViewAt(0);
		getActionBar().addItem(Type.Add).setDrawable(R.drawable.gd_action_bar_refresh);
		getActionBar().addItem(Type.Settings).setDrawable(R.drawable.gd_action_bar_settings);
		getActionBar().addItem(Type.Help).setDrawable(R.drawable.gd_action_bar_help);
		adapter = new ArrayAdapter(this,R.layout.list_item,EVENTS);
		setListAdapter(adapter);
		this.afficherEmploiDuTemps();
	}
	
	
	
	@Override
	public boolean onHandleActionBarItemClick(ActionBarItem item, int position) {
		// TODO Auto-generated method stub
		if (position == 0) miseAJour();
		else if (position == 1) ouvrirPagePreferences();
		else if (position == 2) new AlertDialog.Builder(Main.this).setMessage("Pamplemousse Viewer\nMerci à Samuel pour son aide ! \n\nLe code source de l'application est disponible sur : http://code.google.com/p/pamplemousseviewer\n\nN'hésitez pas à contribuer, c'est du JAVA, c'est le bien :)").setPositiveButton(android.R.string.ok, null).show();
		return super.onHandleActionBarItemClick(item, position);
	}

	public void ouvrirPagePreferences() {
		Main.this.startActivity(new Intent(Main.this,Prefs.class));
	}

	public void loading(String message) {
		try {
			if (dialog == null)  dialog = new ProgressDialog(this);
			if (message == null) {
				dialog.dismiss();
				return;
			}
			dialog.setMessage(message);
			if (!dialog.isShowing()) dialog.show();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public void miseAJour() {
		if (!Prefs.isParametre(this)) {
			Toast.makeText(this, "Vous devez d'abord renseigner vos identifiants", Toast.LENGTH_LONG).show();
			ouvrirPagePreferences();
		}
		else {
			loading("Récupération de l'emploi du temps");
			new MiseAJour(this, this).executer();
		}
	}

	@Override
	public void termine() {
		// TODO Auto-generated method stub
		loading(null);
		afficherEmploiDuTemps();
	}
}