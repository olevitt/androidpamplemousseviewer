package com.estragon.pamplemousseviewer;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class Prefs extends PreferenceActivity	{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.addPreferencesFromResource(R.layout.prefs);
		
	}

	public static String getIdentifiant(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String identifiant = prefs.getString("identifiant", "");
		if (identifiant.equals("")) return identifiant;
		try {
			Integer.parseInt(identifiant);
			identifiant = "id"+identifiant;
		}
		catch (Exception e) {
			
		}
		return identifiant;
	}
	
	public static String getMdp(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String mdp = prefs.getString("mdp", "");
		return mdp;
	}
	
	public static boolean isParametre(Context context) {
		return !getIdentifiant(context).equals("") && !getMdp(context).equals("");
	}
}
