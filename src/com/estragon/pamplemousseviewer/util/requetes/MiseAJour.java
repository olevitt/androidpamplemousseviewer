package com.estragon.pamplemousseviewer.util.requetes;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.estragon.pamplemousseviewer.Main;
import com.estragon.pamplemousseviewer.Prefs;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class MiseAJour {

	public static final int ETAPE_CONNEXION = 0, ETAPE_TOKEN = 1, ETAPE_EMPLOI_DU_TEMPS = 2;
	int etape = 0;
	Context context;
	MAJListener listener;

	public static final AsyncHttpClient client = new AsyncHttpClient();
	public static final String URL = "http://chessdiags.com/pamplemousse/";


	public MiseAJour(Context context,MAJListener listener) {
		this.context = context;
		this.listener = listener;
	}

	public void executer() {
		RequestParams params = new RequestParams();
		params.put("login",Prefs.getIdentifiant(context));
		params.put("mdp",Prefs.getMdp(context));
		client.setTimeout(25000);
		client.get(URL, params , new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(JSONObject arg0) {
				// TODO Auto-generated method stub
				super.onSuccess(arg0);
				
				if (arg0.has("error")) {
					Toast.makeText(context, "Erreur : Merci de verifier que les identifiants rentrés sont valides", Toast.LENGTH_LONG).show();
				}
				else {
					saveToFile(arg0.toString());
				}
				listener.termine();
			}

			public void saveToFile(String data) {
				try {
					FileOutputStream w = context.openFileOutput(Main.FILENAME,Context.MODE_PRIVATE);
					w.write(data.getBytes());
					w.close();
					Toast.makeText(context, "Emploi du temps mis à jour !", Toast.LENGTH_SHORT).show();
				} 
				catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					Log.e("Pamplemousse","Erreur",e);
					Toast.makeText(context, "Erreur : "+e, Toast.LENGTH_SHORT).show();
				}
				catch (IOException e) {
					Log.e("Pamplemousse","Erreur",e);
					Toast.makeText(context, "Erreur : "+e, Toast.LENGTH_SHORT).show();
				}
				catch (Exception e) {
					Log.e("Pamplemousse","Erreur",e);
					Toast.makeText(context, "Erreur : "+e, Toast.LENGTH_SHORT).show();
				}
			}

			@Override
			public void onFailure(Throwable arg0, String arg1) {
				// TODO Auto-generated method stub
				listener.termine();
				Toast.makeText(context, "Connexion au webservice impossible, merci de reessayer", Toast.LENGTH_LONG).show();
				super.onFailure(arg0, arg1);
			}
		});

	}

	public interface MAJListener {
		public void termine();
	}
}
